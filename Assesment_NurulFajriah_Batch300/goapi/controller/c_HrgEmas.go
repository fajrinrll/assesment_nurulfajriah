package controller

import (
	"encoding/json" // package untuk enkode dan mendekode json menjadi struct dan sebaliknya
	"fmt"
	"goapi/models"
	"net/http" // digunakan untuk mengakses objek permintaan dan respons dari api
)

// func enableCors(responseWriter *http.ResponseWriter) {
// 	panic("unimplemented")
// }

type responseCheckHarga struct {
	Error bool                `json:"error"`
	Data  models.CekHargaemas `json:"data"`
}
type responseInputHargaS struct {
	Error  bool   `json:"error"`
	Reffid string `json:"reff_id"`
}
type responseInputHargaF struct {
	Error   bool   `json:"error"`
	Reffid  string `json:"reff_id"`
	Message string `json:"message"`
}
type responseCheckSaldo struct {
	Error bool            `json:"error"`
	Data  models.Rekening `json:"data"`
}

func CekHargaEmas(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json") // set header api to json
	res := responseCheckHarga{}
	res.Error = false
	res.Data = models.CekHargaEmas()
	json.NewEncoder(w).Encode(res)
}

func Inputharga(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json") // set header api to json
	var inharga models.InputHargaemas
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&inharga)
	if err != nil {
		fmt.Println("test error :", err)
	}

	errors, idref := "", ""
	//cek error
	idref, errors = models.Inputharga(inharga)
	if errors == "" { //respons sukses
		ress := responseInputHargaS{}
		ress.Error = false
		ress.Reffid = idref
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		resf := responseInputHargaF{}
		resf.Error = true
		resf.Reffid = idref
		resf.Message = "Kafka not ready"
		json.NewEncoder(w).Encode(resf)
	}

}

func Topup(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	var mtopup models.Topupemas
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&mtopup)
	if err != nil {
		fmt.Println("post body error :", err)
	}

	errors, idref := "", ""
	idref, errors = models.Topup(mtopup)
	if errors == "" { //respons sukses
		ress := responseInputHargaS{}
		ress.Error = false
		ress.Reffid = idref
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		resf := responseInputHargaF{}
		resf.Error = true
		resf.Reffid = idref
		resf.Message = "Harga yang dimasukan berbeda dengan harga topup saat ini"
		json.NewEncoder(w).Encode(resf)
	}
}
func Buyback(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	var buyback models.Buyback
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&buyback)
	if err != nil {
		fmt.Println("post body error :", err)
	}

	errors, idref := "", ""
	idref, errors = models.InputBuyBack(buyback)
	if errors == "" { //respons sukses
		ress := responseInputHargaS{}
		ress.Error = false
		ress.Reffid = idref
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		resf := responseInputHargaF{}
		resf.Error = true
		resf.Reffid = idref
		resf.Message = "Saldo buyback lebih kecil dari juumlah buyback"
		json.NewEncoder(w).Encode(resf)
	}
}
func CekSaldoRek(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	var rekening models.Rekening
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&rekening)
	if err != nil {
		fmt.Println("post body error :", err)
	}

	errors, idref := "", ""
	idref, errors = models.CekSaldoRek(rekening)
	if errors == "" { //respons sukses
		ress := responseCheckSaldo{}
		ress.Error = false
		ress.Data = models.CekSaldo()
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		resf := responseInputHargaF{}
		resf.Error = true
		resf.Reffid = idref
		resf.Message = "No Rekening tidak sesuai"
		json.NewEncoder(w).Encode(resf)
	}
}
