package router

import (
	"goapi/controller"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {

	router := mux.NewRouter()
	srouter := router.PathPrefix("/api/v1").Subrouter() //global router api
	//get with db

	//profile
	srouter.HandleFunc("/check-harga", controller.CekHargaEmas).Methods("GET")
	srouter.HandleFunc("/check-saldo", controller.CekSaldoRek).Methods("POST")
	srouter.HandleFunc("/input-harga", controller.Inputharga).Methods("PUT")
	srouter.HandleFunc("/topup", controller.Topup).Methods("POST")
	srouter.HandleFunc("/buyback", controller.Buyback).Methods("POST")

	return router

}
