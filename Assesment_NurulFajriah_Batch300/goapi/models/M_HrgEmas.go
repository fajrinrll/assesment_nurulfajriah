package models

import (
	"fmt"
	"goapi/config"
	"strconv"

	"github.com/teris-io/shortid"
)

type CekHargaemas struct {
	HARGA_TOPUP   int `json:"harga_topup"`
	HARGA_BUYBACK int `json:"harga_buyback"`
}
type InputHargaemas struct {
	ADMIN_ID      string `json:"admin_id"`
	HARGA_TOPUP   int    `json:"harga_topup"`
	HARGA_BUYBACK int    `json:"harga_buyback"`
}
type Topupemas struct {
	GRAM   string `json:"gram"`
	NO_REK string `json:"no_rek"`
	HARGA  string `json:"harga"`
}
type Rekening struct {
	NO_REK string  `json:"no_rek"`
	SALDO  float32 `json:"saldo"`
}
type Buyback struct {
	GRAM   float32 `json:"Gram"`
	NO_REK string `json:"No_rek"`
	HARGA  int `json:"Harga"`
}

func CekHargaEmas() CekHargaemas {
	db := config.ConnectDB()
	defer db.Close()
	//sql
	sql := "select harga_topup, harga_buyback from harga_emas"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var sc CekHargaemas
	for rows.Next() {
		err = rows.Scan(&sc.HARGA_TOPUP, &sc.HARGA_BUYBACK) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}

	return sc
}

func Inputharga(inharga InputHargaemas) (string, string) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "update kota set nama=$1, id_provinsi=$2 where id=$3"

	_, err := db.Exec(sql, inharga.ADMIN_ID, inharga.HARGA_TOPUP, inharga.HARGA_BUYBACK)
	if err != nil {
		fmt.Println("error add prop : ", err)
	}

	fmt.Print(inharga)
	refid, _ := shortid.Generate()
	sError := ""
	//cek admin
	iAdmin := inharga.ADMIN_ID
	if iAdmin != "a001" {
		sError = "1"
	}
	return refid, sError
}

func Topup(mtopup Topupemas) (string, string) {
	refid, _ := shortid.Generate()
	sError := ""

	//ambil harga topup saat ini

	hTopup := CekHargaEmas().HARGA_TOPUP
	//cek harga topup
	iTopup, _ := strconv.Atoi(mtopup.HARGA) //string to int
	if iTopup == hTopup {
		//insert tbl_topup
		insertTranTopup(mtopup)
		//hitung saldo norek
		tSaldo := getSaldoRek(mtopup.NO_REK)
		//update tbl_rek norek
		updateRekening(mtopup.NO_REK, tSaldo)
	} else {
		sError = "1"
	}
	return refid, sError
}



func getHargaTopUp() int {
	db := config.ConnectDB()
	defer db.Close()
	//sql
	sql := "select harga_topup from harga_emas"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var htopup int
	for rows.Next() {
		err = rows.Scan(&htopup) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}
	return htopup
}

func insertTranTopup(mtopup Topupemas) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "insert into top_up(no_rek,gram,harga)VALUES($1,$2,$3)"
	_, err := db.Exec(sql, mtopup.NO_REK, mtopup.GRAM, mtopup.HARGA)
	if err != nil {
		fmt.Println("sql insert topup : ", err)
	}
}

func getSaldoRek(rek string) float32 {
	db := config.ConnectDB()
	defer db.Close()
	sql := "select sum(NULLIF(gram,'')::float ) from top_up where no_rek=$1"
	rows, err := db.Query(sql, rek)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var saldo float32
	for rows.Next() {
		err = rows.Scan(&saldo)
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}
	return saldo
}

func updateRekening(norek string, saldo float32) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "update rekening set saldo=$1 where no_rek=$2"
	_, err := db.Exec(sql, saldo, norek)
	if err != nil {
		fmt.Println("sql update rek : ", err)
	}
}
func CekSaldo() Rekening {
	db := config.ConnectDB()
	defer db.Close()
	//sql
	sql := "select no_rek,round(saldo::decimal,1) saldo from rekening"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var sc Rekening
	for rows.Next() {
		err = rows.Scan(&sc.NO_REK, &sc.SALDO) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}

	return sc
}
func CekSaldoRek(rekening Rekening) (string, string) {
	refid, _ := shortid.Generate()
	sError := ""

	//ambil harga topup saat ini
	norek := rekening.NO_REK
	//cek harga topup

	if norek == "a001" {
		CekSaldo()
	} else {
		sError = "1"
	}
	return refid, sError
}
func InputBuyBack(buyback Buyback) (string, string) {
	refid, _ := shortid.Generate()
	sError := ""
	saldoGram := CekSaldo().SALDO
	gram := buyback.GRAM
	if saldoGram >= gram {

		//insert table top_up
		//hitung saldo no_rek
		SALDO := CekSaldo().SALDO
		SALDO = SALDO - gram
		updateRekening(buyback.NO_REK, SALDO)
	}else{
		sError="1"
	}
	return refid, sError
}
